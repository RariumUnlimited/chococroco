// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_WINDOW_H_

#define SRC_WINDOW_WINDOW_H_

#include <unordered_map>

#include <Rarium/Window/Window.h>
#include <SFML/Graphics.hpp>

#include "Game/Game.h"
#include "Graphic/Uis/EntityStats.h"
#include "Graphic/Uis/PlayerStats.h"
#include "Window/Action.h"
#include "Window/ActionEventArg.h"
#include "Window/AnimationManager.h"

namespace CC {

/**
 * @brief A window where we will this our game
 */
class Window : public RR::Window {
 public:
    /**
     * @brief Construct and open a new window
     */
    Window();
    /**
     * @brief Main loop iteration, draw things, ...
     */
    void Draw(float delta, sf::RenderTarget& target);

    typedef RR::Event<void, ActionEventArg> ActionEvent;
    typedef std::unordered_map<Action, ActionEvent> ActionEventsMap;
    ActionEventsMap ActionEvents;  ///< A map where each action may have its own event


    static const sf::Vector2u Size;  ///< Size of the window (in pixels)
    static sf::Font Font;  ///< Font used to draw text accross all uis

 protected:
    /**
     * @brief Called when the user presses a key
     * @param is Input state
     * @param k The key pressed
     */
    void HandleKeyPressed(const RR::InputState& is, sf::Keyboard::Key k);
    /**
     * @brief Update the view of the window, according to the player position
     */
    void UpdateView();

    Game game;  ///< Our game logic instance
    ActionManager actionManager;  ///< Action manager of the window
    AnimationManager animationManager;  ///< Animation manager of the window

    EntityStats entityStats;  ///< Ui to display the stats of the entity facing the player
    PlayerStats playerStats;  ///< Ui to display player stats

    sf::View worldView;  ///< View used to draw the world
    sf::View playerStatUiView;  ///< View used to draw the player stat ui
    sf::View otherUiView;  ///< View used to draw other ui (down the player stat ui)
};

}  // namespace CC

#endif  // SRC_WINDOW_WINDOW_H_
