// Copyright RariumUnlimited - Licence : MIT
#include "Window/Action.h"

namespace CC {

ActionManager::ActionManager() {
    keysToActions.insert({
        {sf::Keyboard::Left, Action::MoveLeft},
        {sf::Keyboard::Right, Action::MoveRight},
        {sf::Keyboard::Up, Action::MoveUp},
        {sf::Keyboard::Down, Action::MoveDown},
        {sf::Keyboard::F, Action::Mine},
        {sf::Keyboard::J, Action::Jump},
        {sf::Keyboard::B, Action::Build},
        {sf::Keyboard::E, Action::Use}
    });

    for (std::pair<sf::Keyboard::Key, Action> p : keysToActions) {
        actionsToKeys[p.second] = p.first;
    }
}

Action ActionManager::GetAction(sf::Keyboard::Key key) const {
    KeyMap::const_iterator it = keysToActions.find(key);
    return it != keysToActions.end() ? it->second : Action::Blank;
}

sf::Keyboard::Key ActionManager::GetKey(Action a) const {
    ActionMap::const_iterator it = actionsToKeys.find(a);
    return it != actionsToKeys.end() ? it->second : sf::Keyboard::KeyCount;
}

bool ActionManager::IsAnAction(sf::Keyboard::Key key) const {
    return keysToActions.find(key) != keysToActions.end();
}

}  // namespace CC
