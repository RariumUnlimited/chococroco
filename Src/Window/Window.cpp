// Copyright RariumUnlimited - Licence : MIT
#include "Window/Window.h"

#include <chrono>
#include <ctime>
#include <iostream>
#include <string>

#include "Graphic/Sprite.h"
#include "Graphic/Texture.h"

//#define _RECORDING_

namespace CC {

Window::Window() :
    RR::Window("ChocoCroco",
               sf::VideoMode(Size.x, Size.y),
               sf::Style::Close
#ifdef _RECORDING_
               | sf::Style::Fullscreen
#endif
               ),
    game(*this),
    playerStats(game.GetPlayer(), game.GetBase()) {
    if (Size.x < Size.y)
        throw std::string("Window width cannot be inferior to height");

    worldView.setSize(sf::Vector2f(Size.y, Size.y));

    worldView.setViewport(sf::FloatRect(0.f, 0.f, static_cast<float>(Size.y) /
                                        static_cast<float>(Size.x), 1.f));

    playerStatUiView.setSize(sf::Vector2f(Size.x - Size.y, Size.y * 0.5f));

    float uiWidth = static_cast<float>(Size.x - Size.y) /
                    static_cast<float>(Size.x);

    playerStatUiView.setViewport(sf::FloatRect(1.0f - uiWidth, 0.f,
                                               uiWidth, 0.5f));

    playerStatUiView.setCenter(sf::Vector2f(playerStatUiView.getSize().x / 2,
                                            playerStatUiView.getSize().y / 2));

    otherUiView.setSize(sf::Vector2f(Size.x - Size.y, Size.y * 0.5f));

    otherUiView.setViewport(sf::FloatRect(1.0f - uiWidth, 0.5f,
                                          uiWidth, 0.5f));

    otherUiView.setCenter(sf::Vector2f(otherUiView.getSize().x / 2,
                                       otherUiView.getSize().y / 2));

    if (!TextureCollection::Load())
        Close();

    Font.loadFromFile("./Assets/arial.ttf");

    game.Init();

    for (int i = 0; i < Action::ActionCount; ++i) {
        KeyPressedEvents[actionManager.GetKey(Action(i))] +=
                new RR::KeyEvent::T<Window>(this,&Window::HandleKeyPressed);
    }
}

void Window::HandleKeyPressed(const RR::InputState& is, sf::Keyboard::Key k) {
    Action currentAction = actionManager.GetAction(k);
    if (currentAction != Action::Blank) {
        ActionEventsMap::iterator it =
                ActionEvents.find(currentAction);

        if (it != ActionEvents.end()) {
            bool endTurn;
            ActionEventArg arg(
                        currentAction,
                        k,
                        game,
                        endTurn,
                        animationManager);
            it->second(arg);
            if (endTurn)
                game.Resolve();
        }
    }
}

void Window::Draw(float delta, sf::RenderTarget& target) {
    UpdateView();
    playerStats.Update();
    entityStats.Update(game.GetPlayer().GetFacingEntity());

    animationManager.Update();

    target.setView(worldView);
    target.draw(game);
    target.draw(animationManager);

    target.setView(playerStatUiView);
    target.draw(playerStats);

    target.setView(otherUiView);
    target.draw(entityStats);

    target.setView(target.getDefaultView());
}

void Window::UpdateView() {
    sf::Vector2f center(
                static_cast<float>(
                    game.GetPlayer().Get<sf::Vector2u>("Position").x *
                    Sprite::Size+Sprite::Size/2),
                static_cast<float>(
                    game.GetPlayer().Get<sf::Vector2u>("Position").y *
                    Sprite::Size+Sprite::Size/2));

    if (center.x < worldView.getSize().x / 2.f)
        center.x = worldView.getSize().x / 2.f;

    if (center.x > game.GetWorld().GetSize().x * Sprite::Size -
                   worldView.getSize().x / 2.f)
        center.x = game.GetWorld().GetSize().x * Sprite::Size -
                   worldView.getSize().x / 2.f;

    if (center.y < worldView.getSize().y / 2.f)
        center.y = worldView.getSize().y / 2.f;

    if (center.y > game.GetWorld().GetSize().y * Sprite::Size -
                   worldView.getSize().y / 2.f)
        center.y = game.GetWorld().GetSize().y * Sprite::Size -
                   worldView.getSize().y / 2.f;

    worldView.setCenter(center);
}

const sf::Vector2u Window::Size = sf::Vector2u(640, 480);
sf::Font Window::Font;

}  // namespace CC
