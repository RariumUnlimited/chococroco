// Copyright RariumUnlimited - Licence : MIT
#include "Graphic/Uis/PlayerStats.h"

#include "Graphic/Texture.h"
#include "Graphic/Sprite.h"
#include "Window/Window.h"

namespace CC {

PlayerStats::PlayerStats(Player& player, Base& base) :
    player(player),
    base(base) {
    portrait.setTexture(
                TextureCollection::Get(TextureCollection::Id::SpriteSheet));
    portrait.setTextureRect(Sprite::Get(Sprite::Portrait));
    portrait.setPosition(8, 12);

    name.setString("Gunther");
    name.setFillColor(sf::Color::White);
    name.setPosition(80, 12);
    name.setFont(Window::Font);
    name.setCharacterSize(18);
    name.setStyle(sf::Text::Bold | sf::Text::Underlined);


    ore.setString("Ore : " +
                  std::to_string(player.Get<unsigned int>("Ore")) +
                  "/" + std::to_string(player.Get<unsigned int>("MaxOre")));
    ore.setFillColor(sf::Color::White);
    ore.setPosition(6, 88);
    ore.setFont(Window::Font);
    ore.setCharacterSize(12);

    baseOre.setFillColor(sf::Color::White);
    baseOre.setPosition(6, 102);
    baseOre.setFont(Window::Font);
    baseOre.setCharacterSize(12);

    basePG.setFillColor(sf::Color::White);
    basePG.setPosition(6, 116);
    basePG.setFont(Window::Font);
    basePG.setCharacterSize(12);

    elements.push_back(&portrait);
    elements.push_back(&name);
    elements.push_back(&ore);
    elements.push_back(&baseOre);
    elements.push_back(&basePG);
}

void PlayerStats::Update() {
    ore.setString("Ore : " +
                  std::to_string(player.Get<unsigned int>("Ore")) +
                  "/" + std::to_string(player.Get<unsigned int>("MaxOre")));
    baseOre.setString("Ore in base : " + std::to_string(base.GetAvailableOre()));
    basePG.setString("Free power in base : " +
                     std::to_string(base.GetFreePowerGrid()) + "MW");
}

}  // namespace CC
