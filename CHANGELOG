c5.6 - Building cost and powergrid

* Changed CurrentOre property to Ore
* Added BaseFactory::GetCost method to get the cost in ore of a building
* Added Base::GetAvailable method to get the amount of ore available in all silos
* Added Base::RemoveOre method to remove a specific amount of ore from the silos
* Added build cost handling in Game::PlayerBuild method
* Added Base::GetFreePowerGrid to get how many free power the base has
* Added PG (PowerGrid) property to buildings to have how many power it use or produce
* Added PG handling in Game::PlayerBuild method


c5.5 - View an entity information

* World view a now a square of 480x480 pixel
* Removed game overlay, player stats are now display at the right of the screen
* Added a ui to display the information of the entity facing the player at the right of the screen
* Added TextFunc property to Entity to get an information text
* Added Name property to Entity

c5.4 - Saving and loading the base

* Added Building class
* OreSilo now inherits Building
* Added Core building class
* Base is now a collection of Building* (was Entity*)
* Added BaseFactory class
* Added Split function to split a string in Tool.h

c5.3 - Interacting with our base

* Added Use action
* Use function are std::function<void(ActionEventArg) and are stored as Entity property with ID PlayerUseFunc.
* Added TransferOre method to Silo as Use function, they will transfer ore from the player to the silo.

c5.2 - Our first buildings

* Added Base class
* Added OreSilo class
* Added SetupBase method to World that will add base buildings to the world
* Game::Load will now call World::SetupBase when loading the base
* Added Build action that will create an OreSilo if possible in front of the player (only at base)

c5.1 - Adventure and Base mode

* Added Jump action that will cause the player to jump (as in warp jump) from base to an adventure
* World::GenerateTiles now take a boolean arguement to allow or deny randomness when generating the size of the world
* Game::Load will only call World::GenerateEntities when loading an adventure
* Added bool attribute to Game to know if the player is in an adventure or not

i1 - Code Review

* Heavy code modification to try to comply to google c++ coding style guide

c4.4 - Floating combat text

* Added Animation class
* Added AnimationManager class, and one instance as Window attribute
* Added AnimationManager attribute to ActionEventArg
* Added FloatingCombatText class (an animation)
* Now play a floating combat text when the player mine an asteroid

c4.3 - A GUI overlay to display player information

* Added Ui class to represent user interfaces
* Added GameOverlay class that display information about the player
* Added a GameOverlay attribute to Game
* Game::DisplayPlayerInfo now switch the overlay on and off

c4.2 - Creating and mining asteroids

* World now has a generator attribute and it is used by Generate*() methods
* Added GenerateEntities method to World. Dynamic allocation, deallocation is handled by Game
* Added PropertyManager class
* Removed attributes from Entity class, now using PropertyManager
* Added Mine and DisplayInfo actions
* Added DisplayPlayerInfo method to Game
* Added Mine method to player
* Added CurrentOre and MaxOre property to Player

c4.1 - Tool : Property manager

* Added PropertyManager class
* Entity now inherits from PropertyManager
* Entity and Player attributes have been move to the PropertyManager

c3.2 - Generating a bigger world

* World now has a random size (from the size of the window to twice the window)
* Added UpdateView method to window so that the view follow the player

c3.1 - Basic random generation

* World now have a global background instead of one per tile
* Tile::BgSprite removed
* World constructor no longer generate a world, to generate a new world, call GenerateTiles and GenerateBackground
* Changed a lot of int to unsigned int and sf::Vector2i to sf::Vector2u (sizes, positions, ...)
* World::CanMove now check that the provided entity is at its right position in the world
* Texture::TileSize has been to a globale constant in sprite.h called SpriteSize
* Changed Texture class name to TextureCollection, it now load all textures
* Added Texture enum to list all available textures

c2.7 - Rotating the player sprite when moving

* The player sprite will correctly rotate when moving
* Added bool attribute to ActionEventArg to allow action event handler to tell that the action ended the turn

c2.6 - Moving the player

* Added Window reference in Game class
* Added World and Window getter in Game
* Added CanMove and Move method to World
* Added Setter for position in Entity
* Added Game reference to Player
* Added Move methods to Player
* Changed the way Tile are drawn, now Tile inherits from sf::Transformable to set its window position and is it reflected in sf::RenderStates.transform when drawn. Tile.BgSprite and Entity.sprite position will need to always be (0,0) to be drawn at the correct position.


c2.5 - Player input and associated actions

* Added Action enum and ActionManager class : keyboard keys are now map to actions
* Added an ActionManager to the Window class
* Change the way to handle events that correspond to an action (using a map Action->Event)


c2.4 - Emulation C# delegates in c++

* Added Event class to emulate C# delegates in c++
* Change the call to TakeScreenshot to use the newly Event class

c2.3 - The player and the game entity class

* Added 8 new background variation
* Change world generation, sprite use is either LILDBackground or LIMDBackground
* Added Entity class
* Added Player class that inherits from Entity
* Added Entity to the world

c2.2 - The world

* Change Window::width and Window::height from int to sf::Vector2i, and made it public and static
* Added World class and Tile struct
* Added World instance to Game class
* Changed ship position to match a tile
