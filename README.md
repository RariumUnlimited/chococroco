ARCHIVED
========

This project has been archived and will not be updated anymore, it has been integrated in the Miraverse project. It only remain here for historic purpose.

ChocoCroco
==========

About
-----

Turn-by-turn space game created from scratch in c++ using SFML.

Check the [Wiki](https://gitlab.com/RariumUnlimited/chococroco/wikis/home), it's a bit like a liveblog.

Build
-----

- Install [SFML 2.5.1](https://www.sfml-dev.org/download/sfml/2.5.1/)
- Clone using the `--recursive` flag to grab the dependencies

```
mkdir build
cd build
cmake ..
make 
cp -r ./Assets ./
./ChocoCroco
```

License
-------

All ChocoCroco source code and assets are provided under the MIT license (See LICENSE file)
